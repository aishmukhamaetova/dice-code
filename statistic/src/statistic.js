const {v4: uuid} = require('uuid')
const {knex} = require('./knex')

//https://stackoverflow.com/questions/60690035/adding-a-column-to-an-existing-table-in-node-js-knex

exports.updateStatistic = async ({user, amount, payout, game}) => {
  console.log(`updateStatistic insert into statistic`, user, amount, payout, game)

  const result = await knex.raw(
    `
      insert into statistic (
        "id", "user", "wagered", "profit", "game"
      ) values (
        :id, :user, :wagered, :profit, :game
      )
      on conflict ("user", "game") do update
      set 
      wagered = statistic.wagered + :wagered,
      profit = statistic.profit + :profit
    `,
    {
      id: uuid(),
      user,
      wagered: amount,
      profit: payout - amount,
      game,
    },
  )
}

exports.getStatistic = async ({user}) => {
  const statistic = await knex('statistic').where('user', user)
  return statistic
}

//https://github.com/knex/knex/issues/32
//https://stackoverflow.com/questions/35888012/use-multiple-conflict-target-in-on-conflict-clause
